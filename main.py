import sys
from mywindow import MyWindow
from PyQt5.QtWidgets import QApplication

def main():
    app = QApplication(sys.argv)
    gui = MyWindow()
    gui.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()