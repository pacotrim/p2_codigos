## Modelador
---
O modelador será chamado ao executar o arquivo main.py.
Para usar a curva bezier, basta clicar no botão da curva. Após isso, a primeira ação de clicar e arrastar vai definir o começo e fim da curva, e a segunda vai definir o ponto de controle da curva. O ponto de controle é atualizado dinamicamente para que possa ser visualizado antes de soltar o botão do mouse.

Para criar entradas para PVI ou PVC, é necessário desenhar um polígono, clicar no ícone de selecionar(ponteiro de mouse), escolher a face desejada e o tipo de problema desejado. As arestas do polígono serão destacadas uma por uma, e as condições iniciais/de contorno serão pedidas para cada uma. Depois de preenchidas, serão pedidas quaisquer outras condições necessárias e o arquivo será gerado.

## Resolvedor PVI
O resolvedor de PVI será chamado ao executar o arquivo pvi.py.
Para resolver o problema visto em sala, não é necessário nenhum argumento. Ele será resgatado do arquivo `in_pvi.json` e o resultado será guardado em `out_pvi.json`
Para utilizar outra entrada, basta executar `python pvi.py <arquivo_de_entrada> <arquivo_de_saída>`

## Resolvedor PVC
O resolvedor de PVC será chamado ao executar o arquivo pvc.py.
Para resolver o problema visto em sala, não é necessário nenhum argumento. Ele será resgatado do arquivo `in_pvc.json` e o resultado será guardado em `out_pvc.json`
Para utilizar outra entrada, basta executar `python pvc.py <arquivo_de_entrada> <arquivo_de_saída>`