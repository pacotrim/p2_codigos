from typing import Tuple
import numpy as np


def read_json(inp_path: str) -> Tuple[int, list[list[int]], list[list[int]]]:
    from json import load
    num_parts = 0
    temps = [[0]]
    conects = [[0]]
    with open(inp_path, 'r') as entry:
        entry_object = load(entry)
        temps = entry_object['temps']
        conects = entry_object['conects']
        num_parts = len(temps)
    return num_parts, temps, conects


def save_json(out_path:str, results: dict[str,list[float]]) -> None:
    from json import dump
    with open(out_path,'w') as output:
        dump(results,output)
    return None

def main(inp_path: str,out_path:str) -> None:
    from json import dump
    coeficientes: np.ndarray = np.array([1, 1, 1, 1, -4])

    num_parts, temps, conects = read_json(inp_path)

    A: np.ndarray = np.zeros(shape=(num_parts, num_parts))
    b: np.ndarray = np.zeros(shape=(num_parts))

    for i in range(num_parts):
        for j in range(len(conects[i])):
            if conects[i][j] > 0:
                A[i, conects[i][j]-1] = coeficientes[j]
        if temps[i][0] == 1:
            A[i, :] = 0
            A[i, i] = 1
            b[i] = temps[i][1]

    result = {'temps': np.linalg.solve(A, b).tolist()}
    return save_json(out_path, result)


if __name__ == "__main__":
    import sys
    if len(sys.argv)>1:
        main(*sys.argv[1:])
    else:
        main('in_pvc.json','out_pvc.json')
