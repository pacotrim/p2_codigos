from PyQt5.QtWidgets import *

class PartDistForm(QDialog):
 
    def __init__(self):
        super(PartDistForm, self).__init__()
 
        self.setWindowTitle("Particle Distance")
 
        self.setGeometry(100, 100, 300, 400)
 
        self.formGroupBox = QGroupBox("Enter Particle Distance")
 
        self.dist = QLineEdit()
 
        self.createForm()
 
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.getInfo)
 
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(self.buttonBox)
 
        self.setLayout(mainLayout)
 
    def getInfo(self):
        self.close()
 
    def createForm(self):
        layout = QFormLayout()
        layout.addRow(QLabel("Particle Distance"), self.dist)
 
        self.formGroupBox.setLayout(layout)

class PVCSegForm(QDialog):
 
    def __init__(self):
        super(PVCSegForm, self).__init__()
 
        self.setWindowTitle("Temperature")
 
        self.setGeometry(100, 100, 300, 400)
 
        self.formGroupBox = QGroupBox("Enter Temperature for highlighted segment")
 
        self.temp = QLineEdit()
 
        self.createForm()
 
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.getInfo)
 
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(self.buttonBox)
 
        self.setLayout(mainLayout)
 
    def getInfo(self):
        self.close()
 
    def createForm(self):
        layout = QFormLayout()
        layout.addRow(QLabel("Temperature"), self.temp)
 
        self.formGroupBox.setLayout(layout)

class PVISegCondForm(QDialog):
    def __init__(self):
        super(PVISegCondForm, self).__init__()
 
        self.setWindowTitle("Segment Conditions")
 
        self.setGeometry(100, 100, 300, 400)
 
        self.formGroupBox = QGroupBox("Enter params for highlighted segment")
 
        self.forca_x = QLineEdit()
        self.forca_y = QLineEdit()
        self.restr_x = QLineEdit()
        self.restr_y = QLineEdit()
 
        self.createForm()
 
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.getInfo)
 
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(self.buttonBox)
 
        self.setLayout(mainLayout)
 
    def getInfo(self):
        self.close()
 
    def createForm(self):
 
        layout = QFormLayout()
 
        layout.addRow(QLabel("Força X"), self.forca_x)
        layout.addRow(QLabel("Força Y"), self.forca_y)
        layout.addRow(QLabel("Restr X"), self.restr_x)
        layout.addRow(QLabel("Restr Y"), self.restr_y)
 
        self.formGroupBox.setLayout(layout)

class PVIParamsForm(QDialog):
    def __init__(self):
        super(PVIParamsForm, self).__init__()
 
        self.setWindowTitle("Initial Conditions")
 
        self.setGeometry(100, 100, 300, 400)
 
        self.formGroupBox = QGroupBox("Initial Conditions")
 
        self.massa = QLineEdit()
        self.raio = QLineEdit()
        self.n = QLineEdit()
        self.h = QLineEdit()
        self.kspr = QLineEdit()
 
        self.createForm()
 
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.getInfo)
 
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(self.buttonBox)
 
        self.setLayout(mainLayout)
 
    def getInfo(self):
        self.close()
 
    def createForm(self):
 
        layout = QFormLayout()
 
        layout.addRow(QLabel("Massa"), self.massa)
        layout.addRow(QLabel("Raio"), self.raio)
        layout.addRow(QLabel("N"), self.n)
        layout.addRow(QLabel("h"), self.h)
        layout.addRow(QLabel("Kspr"), self.kspr)
 
        self.formGroupBox.setLayout(layout)