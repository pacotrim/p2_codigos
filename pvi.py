import numpy as np
import matplotlib.pyplot as plt
from typing import NamedTuple


class InitialConditions(NamedTuple):
    raio: float
    mass: float
    kspr: float
    num_par: int
    ndofs: int
    N: int
    h: float
    xs: np.ndarray
    ys: np.ndarray
    F: np.ndarray
    restrs: np.ndarray
    connect: list[list[int]]


def read_json(inp_path: str) -> InitialConditions:
    from json import load
    with open(inp_path, 'r') as inp:
        obj = load(inp)
    coords = obj['coords']
    xs = np.array([coord[0] for coord in coords])
    ys = np.array([coord[1] for coord in coords])
    raio = obj['raio']
    mass = obj['mass']
    kspr = obj['kspr']
    num_par = xs.shape[0]
    ndofs = num_par*2
    N = obj['N']
    h = obj['h']
    F = np.array(obj['f']).reshape((ndofs))
    restrs = np.array(obj['restrs']).reshape(ndofs)
    connect = obj['connect']
    return InitialConditions(raio, mass, kspr, num_par, ndofs, N, h, xs, ys, F, restrs, connect)

def save_json(out_path: str, results: np.ndarray) -> None:
    from json import dump
    results_obj = {"resultado":results.tolist()}
    with open(out_path,'w') as out_file:
        dump(results_obj, out_file)
    return None


def find_res_idx(F: np.ndarray) -> int:
    counter = 0
    sum_value = 0
    for i in range(len(F)):
        if (F[i] != 0):
            sum_value += i
            counter += 1
    resp = int(sum_value/counter)
    if (counter % 2 == 0):
        return resp + 1
    return resp


def main(inp_path: str, out_path: str) -> None:

    raio, mass, kspr, ne, ndofs, N, h, x0, y0, F, restrs, conect = read_json(
        inp_path)

    u: np.ndarray = np.zeros(ndofs)
    v: np.ndarray = np.zeros(ndofs)
    a: np.ndarray = np.zeros(ndofs)
    f_int: np.ndarray = np.zeros(ndofs)

    res: np.ndarray = np.zeros(N)

    a = (F-f_int)/mass
    for i in range(N):
        f_int *= 0
        v += a * (h/2)
        u += (v*h)
        for j in range(ne):
            if (restrs[j*2] == 1):
                u[j*2] = 0
            if (restrs[(j*2)+1] == 1):
                u[(j*2)+1] = 0
            xj = x0[j] + u[j*2]
            yj = y0[j] + u[(j*2)+1]
            for k in range(conect[j][0]):
                ki = conect[j][1+k] - 1
                xk = x0[ki] + u[ki*2]
                yk = y0[ki] + u[(ki*2)+1]
                dX = xj-xk
                dY = yj-yk
                di = np.sqrt(dX*dX+dY*dY)
                d2 = (di - 2*raio)
                dx = d2*dX/di
                dy = d2*dY/di
                f_int[2*j] += kspr*dx
                f_int[(2*j)+1] += kspr*dy
        a = (F - f_int)/mass
        v += a * (h/2)
        res[i] = u[find_res_idx(F)]
    plt.plot(res)
    plt.show()
    return save_json(out_path,res)


if __name__ == '__main__':
    from sys import argv
    if len(argv) > 1:
        main(*argv[1:])
    else:
        main('in_pvi.json', 'out_pvi.json')
