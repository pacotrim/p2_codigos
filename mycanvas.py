from PyQt5.QtOpenGL import *
from OpenGL.GL import *
from PyQt5.QtCore import *
from math import sqrt
from enum import Enum
from hetool.include.hetool import Hetool

#quadratic bezier coordinates
#p0 is the starting point, p2 the ending point, p1 is the control point
def xBez(p0,p1,p2, t: float) -> float:
    a = ((1-t)**2)*p0.x()
    b = 2 * (1-t) * t * p1.x()
    c = t**2 * p2.x()
    result = a + b + c
    return result

def yBez(p0,p1,p2, t:float) -> float:
    a = ((1-t)**2)*p0.y()
    b = 2 * (1-t) * t * p1.y()
    c = t**2 * p2.y()
    result = a + b + c
    return result


class State(Enum):
    SELECT = 0
    LINE = 1
    BEZIER_SEGMENT = 2
    BEZIER_CONTROL = 3

class MyCanvas(QGLWidget):
    def __init__(self):
        super(MyCanvas, self).__init__()

        self.m_w = 0  # width: GL canvas horizontal size
        self.m_h = 0  # height: GL canvas vertical size
        self.m_L = -1000.0
        self.m_R = 1000.0
        self.m_B = -1000.0
        self.m_T = 1000.0
        self.list = None
        self.m_buttonPressed = False
        self.m_pt0: QPointF = QPointF(0, 0)
        self.m_pt1: QPointF = QPointF(0, 0)
        self.state = State.SELECT
        self.incomplete_curve = []

    def enableBezierMode(self):
        self.state = State.BEZIER_SEGMENT
        self.reset()
        self.update()
    
    def enableLineMode(self):
        self.state = State.LINE
        self.reset()
        self.update()
    
    def enableSelectMode(self):
        self.state = State.SELECT
        self.reset()
        self.update()
    
    def delete_patches(self):
        Hetool.delSelectedEntities()
        Hetool.unSelectAll()
        self.reset()
        self.update()

    def export_patch(self):# -> list[list[QPointF,str]]:
        if Hetool.isEmpty() or len(Hetool.getSelectedPatches()) == 0:
            return -1
        else:
            patch = Hetool.getSelectedPatches()[0]
            return patch
    
    def initializeGL(self):
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT)
        self.list = glGenLists(1)

    def resizeGL(self, _w, _h):
        self.m_w = _w
        self.m_h = _h

        if Hetool.isEmpty():
            self.scaleWorldWindow(1.0)
        else:
            self.fitWorldToViewport()

        glViewport(0, 0, _w, _h)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(self.m_L, self.m_R, self.m_B, self.m_T, -1.0, 1.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def paintGL(self):
        # clear the buffer with the current clear color
        glClear(GL_COLOR_BUFFER_BIT)
        # draw the model
        if Hetool.isEmpty():
            return
        glCallList(self.list)
        glDeleteLists(self.list, 1)
        self.list = glGenLists(1)
        glNewList(self.list, GL_COMPILE)
        if not Hetool.isEmpty():
            patches = Hetool.getPatches()
            for patch in patches:
                if patch.isDeleted == False:
                    if patch.isSelected():
                        glColor3f(1,1,1)
                    else:
                        glColor3f(0.8,0.8,0.8)
                    triangs = Hetool.tessellate(patch)
                    for triangle in triangs:
                        glBegin(GL_TRIANGLES)
                        for pt in triangle:
                            glVertex2d(pt.getX(), pt.getY())
                        glEnd()
            curves = Hetool.getSegments()
            glColor3f(0.0, 0.0, 1.0)  # blue
            for curv in curves:
                glBegin(GL_LINE_STRIP)
                pts = curv.getPointsToDraw()
                for pt in pts:
                    glVertex2f(pt.getX(), pt.getY())
                glEnd()
            glColor3f(1.0,0.0,0.0) #red
            glBegin(GL_LINE_STRIP)
            if self.state != State.BEZIER_CONTROL:
                pt0_U = self.convertPtCoordsToUniverse(self.m_pt0)
                pt1_U = self.convertPtCoordsToUniverse(self.m_pt1)
                glVertex2f(pt0_U.x(), pt0_U.y())
                glVertex2f(pt1_U.x(), pt1_U.y())
            else:
                for pt in self.incomplete_curve:
                    glVertex2f(pt.x(),pt.y())
            glEnd()
            for patch in patches:
                if patch.isDeleted == False:
                    if patch.isSelected():
                        for seg in patch.getSegments():
                            if seg.isSelected():
                                glColor3f(0,0,0)
                                glBegin(GL_LINE_STRIP)
                                for point in seg.getPointsToDraw():
                                    glVertex2f(point.x,point.y)
                                glEnd()
        glEndList()

    def fitWorldToViewport(self):
        if Hetool.isEmpty():
            return

        self.m_L, self.m_R, self.m_B, self.m_T = Hetool.getBoundBox()
        self.scaleWorldWindow(1.1)

    def scaleWorldWindow(self, _scaleFactor):
        cx = 0.5*(self.m_L + self.m_R)
        cy = 0.5*(self.m_B + self.m_T)
        dx = (self.m_R - self.m_L)*_scaleFactor
        dy = (self.m_T - self.m_B)*_scaleFactor

        ratioVP = self.m_h/self.m_w
        if dy > dx*ratioVP:
            dx = dy/ratioVP
        else:
            dy = dx*ratioVP

        self.m_L = cx - 0.5*dx
        self.m_R = cx + 0.5*dx
        self.m_B = cy - 0.5*dy
        self.m_T = cy + 0.5*dy

        self.m_heTol = 0.005*(dx+dy)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(self.m_L, self.m_R, self.m_B, self.m_T, -1.0, 1.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def panWorldWindow(self, _panFacX, _panFacY):
        # Compute pan distances in horizontal and vertical directions.
        panX = (self.m_R - self.m_L) * _panFacX
        panY = (self.m_T - self.m_B) * _panFacY
        # Shift current window.
        self.m_L += panX
        self.m_R += panX
        self.m_B += panY
        self.m_T += panY
        # Establish the clipping volume by setting up an
        # orthographic projection
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(self.m_L, self.m_R, self.m_B, self.m_T, -1.0, 1.0)

    def convertPtCoordsToUniverse(self, _pt):
        dX = self.m_R - self.m_L
        dY = self.m_T - self.m_B
        mX = _pt.x() * dX / self.m_w
        mY = (self.m_h - _pt.y()) * dY / self.m_h
        x = self.m_L + mX
        y = self.m_B + mY
        return QPointF(x, y)

    def mousePressEvent(self, event):
        match self.state:
            case State.SELECT:
                pt_univ = self.convertPtCoordsToUniverse(event.pos())
                Hetool.selectPick(pt_univ.x(), pt_univ.y(), 10)
                self.update()
            case State.BEZIER_SEGMENT | State.LINE:
                self.m_buttonPressed = True
                self.m_pt0 = event.pos()
            case _:
                self.m_buttonPressed = True

    def mouseMoveEvent(self, event):
        if self.m_buttonPressed:
            if self.state == State.BEZIER_CONTROL:
                pt0_U = self.convertPtCoordsToUniverse(self.m_pt0)
                pt1_U = self.convertPtCoordsToUniverse(self.m_pt1)
                ptCurr_U = self.convertPtCoordsToUniverse(event.pos())
                self.incomplete_curve = []
                nSampling = 100
                for i in range(1,nSampling+1):
                    t = i/nSampling
                    result_x = xBez(pt0_U,ptCurr_U,pt1_U,t)
                    result_y = yBez(pt0_U,ptCurr_U,pt1_U,t)
                    curr_point = QPointF(result_x,result_y)
                    self.incomplete_curve.append(curr_point)
            elif self.state == State.LINE or self.state == State.BEZIER_SEGMENT:
                self.m_pt1 = event.pos()
        self.update()

    def mouseReleaseEvent(self, event):
        if self.m_pt0 == QPointF() or self.m_pt1 == QPointF():
            self.reset()
            self.update()
        pt0_U = self.convertPtCoordsToUniverse(self.m_pt0)
        pt1_U = self.convertPtCoordsToUniverse(self.m_pt1)
        _, xp,yp = Hetool.snapToPoint(pt0_U.x(),pt0_U.y(),20)
        pt0_U.setX(xp)
        pt0_U.setY(yp)
        _, xp,yp = Hetool.snapToPoint(pt1_U.x(),pt1_U.y(),20)
        pt1_U.setX(xp)
        pt1_U.setY(yp)
        if self.state == State.BEZIER_SEGMENT:
            self.state = State.BEZIER_CONTROL
        elif self.state == State.BEZIER_CONTROL:
            segment = []
            for pt in self.incomplete_curve:
                segment.append(pt.x())
                segment.append(pt.y())
            Hetool.insertSegment(segment)
            self.reset()
        elif self.state == State.LINE:
            Hetool.insertSegment([pt0_U.x(),pt0_U.y(),pt1_U.x(),pt1_U.y()])
            self.reset()
        else:
            self.reset()
        self.update()

    def reset(self):
        self.m_pt0.setX(0)
        self.m_pt0.setY(0)
        self.m_pt1.setX(0)
        self.m_pt1.setY(0)
        self.m_buttonPressed = False
        self.incomplete_curve = []
        if self.state == State.BEZIER_CONTROL:
            self.state = State.BEZIER_SEGMENT