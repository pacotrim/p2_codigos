from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from mycanvas import MyCanvas
from forms import *
from hetool.geometry.point import Point
from math import inf
from json import dump

class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.setGeometry(100,100,600,400)
        self.setWindowTitle("MyBezier")
        self.canvas = MyCanvas()
        self.setCentralWidget(self.canvas)
        # create a Toolbar
        tb = self.addToolBar("File")
        select = QAction(QIcon("icons/cursor.png"),"select",self)
        tb.addAction(select)
        line = QAction(QIcon("icons/line.png"),"line",self)
        tb.addAction(line)
        bezier = QAction(QIcon("icons/curve.png"),"bezier",self)
        tb.addAction(bezier)
        fit = QAction(QIcon("icons/fit.png"),"fit",self)
        tb.addAction(fit)
        delete = QAction(QIcon("icons/delete.png"),"delete",self)
        tb.addAction(delete)
        export_pvi = QAction("Export PVI",self)
        tb.addAction(export_pvi)
        export_pvc = QAction("Export PVC",self)
        tb.addAction(export_pvc)
        tb.actionTriggered[QAction].connect(self.tbpressed)

    def proccess_patch(self,patch, mode):
        if patch == -1:
            print("error")
            return 0
        if mode == "pvi":
            dist = PartDistForm()
            dist.exec()
            dist = int(dist.dist.text())
            all_points = []
            for segment in patch.getSegments():
                segment.setSelected(True)
                self.canvas.update()
                form = PVISegCondForm()
                form.exec()
                segment.setSelected(False)
                self.canvas.update()
                restr_x = float(form.restr_x.text())
                restr_y = float(form.restr_y.text())
                forca_x = float(form.forca_x.text())
                forca_y = float(form.forca_y.text())
                all_points.append([segment.getXinit(),segment.getYinit(),restr_x,restr_y,forca_x,forca_y])
                segment_length = segment.length(0,1)
                dx = (segment.getXend() - segment.getXinit())/segment_length
                dy = (segment.getYend() - segment.getYinit())/segment_length
                for i in range(1,int(segment_length),dist):
                    all_points.append([segment.getXinit()+((i*dist)*dx),segment.getYinit()+((i*dist)*dy),restr_x,restr_y,forca_x,forca_y])
                all_points.append([segment.getXend(),segment.getYend(),restr_x,restr_y,forca_x,forca_y])
            xmin,xmax,ymin,ymax = patch.getBoundBox()
            xmin = int(xmin)
            xmax = int(xmax)
            ymin = int(ymin)
            ymax = int(ymax)
            for i in range(xmin+1,xmax,dist):
                for j in range(ymin+1,ymax,dist):
                    point_attempt = Point(i,j)
                    if patch.isPointInside(point_attempt):
                        all_points.append([i,j,0,0,0,0])
            init_cond = PVIParamsForm()
            init_cond.exec()
            massa = float(init_cond.massa.text())
            raio = float(init_cond.raio.text())
            n = float(init_cond.n.text())
            h = float(init_cond.h.text())
            kspr = float(init_cond.kspr.text())
            connect = []
            for i in range(len(all_points)):
                idx_up = -1
                idx_down = -1
                idx_left = -1
                idx_right = -1
                min_dist_up = inf
                min_dist_down = inf
                min_dist_left = inf
                min_dist_right = inf
                for j in range(len(all_points)):
                    if i != j:
                        curr_dist = Point.euclidiandistance(Point(all_points[j][0],all_points[j][1]),Point(all_points[i][0],all_points[i][1]))
                        if all_points[j][0] > all_points[i][0]:
                            if curr_dist < min_dist_right and all_points[j][1] == all_points[i][1]:
                                min_dist_right = curr_dist
                                idx_right = j+1
                        else:
                            if curr_dist < min_dist_left and all_points[j][1] == all_points[i][1]:
                                min_dist_left = curr_dist
                                idx_left = j+1
                        if all_points[j][1] > all_points[i][1] and all_points[j][0] == all_points[i][0]:
                            if curr_dist < min_dist_up:
                                min_dist_up = curr_dist
                                idx_up = j+1
                        else:
                            if curr_dist < min_dist_down and all_points[j][1] == all_points[i][1]:
                                min_dist_down = curr_dist
                                idx_down = j+1
                curr_connect = [idx for idx in (idx_up,idx_down,idx_left,idx_right) if idx != -1]
                curr_connect.insert(0,len(curr_connect))
                for _ in range(5-len(curr_connect)):
                    curr_connect.append(0)
                connect.append(curr_connect)

            results = {"raio":raio, "mass":massa, "kspr": kspr, "N":n, "h":h,"f":[],"restrs":[],"coords":[],"connect":[]}
            for idx in range(len(all_points)):
                i = all_points[idx]
                results["coords"].append([i[0],i[1]])
                results["restrs"].append([i[2],i[3]])
                results["f"].append([i[4],i[5]])
                results["connect"].append(connect[idx])
            with open("entrada_pvi.json","w") as out:
                dump(results,out)
        else:
            dist = PartDistForm()
            dist.exec()
            dist = int(dist.dist.text())
            all_points = []
            for segment in patch.getSegments():
                segment.setSelected(True)
                self.canvas.update()
                form = PVCSegForm()
                form.exec()
                segment.setSelected(False)
                self.canvas.update()
                temp = float(form.temp.text())
                all_points.append([segment.getXinit(),segment.getYinit(),1,temp])
                segment_length = segment.length(0,1)
                dx = (segment.getXend() - segment.getXinit())/segment_length
                dy = (segment.getYend() - segment.getYinit())/segment_length
                for i in range(1,int(segment_length),dist):
                    all_points.append([segment.getXinit()+((i*dist)*dx),segment.getYinit()+((i*dist)*dy),1,temp])
                all_points.append([segment.getXend(),segment.getYend(),1,temp])
            xmin,xmax,ymin,ymax = patch.getBoundBox()
            xmin = int(xmin)
            xmax = int(xmax)
            ymin = int(ymin)
            ymax = int(ymax)
            for i in range(xmin+1,xmax,dist):
                for j in range(ymin+1,ymax,dist):
                    point_attempt = Point(i,j)
                    if patch.isPointInside(point_attempt):
                        all_points.append([i,j,0,0])
            connect = []
            for i in range(len(all_points)):
                idx_up = -1
                idx_down = -1
                idx_left = -1
                idx_right = -1
                min_dist_up = inf
                min_dist_down = inf
                min_dist_left = inf
                min_dist_right = inf
                for j in range(len(all_points)):
                    if i != j:
                        curr_dist = Point.euclidiandistance(Point(all_points[j][0],all_points[j][1]),Point(all_points[i][0],all_points[i][1]))
                        if all_points[j][0] > all_points[i][0]:
                            if curr_dist < min_dist_right and all_points[j][1] == all_points[i][1]:
                                min_dist_right = curr_dist
                                idx_right = j+1
                        else:
                            if curr_dist < min_dist_left and all_points[j][1] == all_points[i][1]:
                                min_dist_left = curr_dist
                                idx_left = j+1
                        if all_points[j][1] > all_points[i][1] and all_points[j][0] == all_points[i][0]:
                            if curr_dist < min_dist_up:
                                min_dist_up = curr_dist
                                idx_up = j+1
                        else:
                            if curr_dist < min_dist_down and all_points[j][1] == all_points[i][1]:
                                min_dist_down = curr_dist
                                idx_down = j+1
                curr_connect = [idx if idx != -1 else 0 for idx in (idx_up,idx_down,idx_left,idx_right)]
                curr_connect.append(i+1)
                connect.append(curr_connect)

            results = {"temps":[], "conects":[]}
            for idx in range(len(all_points)):
                i = all_points[idx]
                results["temps"].append([i[2],i[3]])
            with open("entrada_pvc.json","w") as out:
                dump(results,out)

    def tbpressed(self,a):
        self.canvas.reset()
        self.canvas.update()
        match a.text():
            case "fit":
                self.canvas.fitWorldToViewport()
            case "bezier":
                self.canvas.enableBezierMode()
            case "line":
                self.canvas.enableLineMode()
            case "delete":
                self.canvas.delete_patches()
            case "select":
                self.canvas.enableSelectMode()
            case "Export PVI":
                self.proccess_patch(self.canvas.export_patch(),"pvi")
            case "Export PVC":
                self.proccess_patch(self.canvas.export_patch(),"pvc")
